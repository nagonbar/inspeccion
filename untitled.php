<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/main.min.css">
    <link rel="stylesheet" href="assets/lib/animate.css/animate.min.css">
    <link rel="stylesheet" href="assets/lib/validationEngine/css/validationEngine.jquery.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/lib/datepicker/css/datepicker.css"> 

</head>

<body>
    <form class="form-horizontal" id="popup-validation" name="form1" action="" method="post" autocomplete="off" enctype="multipart/form-data">
    <table width="500" border="1" cellspacing="10" cellpadding="10">
      <tr>
        <td>NOMBRE</td>
        <td>
        <input type="text" name="textfield" id="textfield" class="form-control top validate[required]"></td>
      </tr>
      <tr>
        <td>FECHA</td>
        <td>
        <input type="text" name="textfield2" id="textfield2" class="form-control top validate[required,custom[integer]]"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="button" id="button" class="btn btn-danger btn-grad" value="Enviar"></td>
      </tr>
    </table>
    </form>
</body>
</html>
    <!--jQuery 2.1.1 -->
    <script src="assets/lib/jquery/jquery.min.js"></script>
    <script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      (function($) {
        $(document).ready(function() {
          $('.list-inline li > a').click(function() {
            var activeForm = $(this).attr('href') + ' > form';
            //console.log(activeForm);
            $(activeForm).addClass('animated fadeIn');
            //set timer to 1 seconds, after that, unload the animate animation
            setTimeout(function() {
              $(activeForm).removeClass('animated fadeIn');
            }, 1000);
          });
        });
      })(jQuery);
    </script>


    <!--jQuery 2.1.1 -->

    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>

    <!--Bootstrap -->


    <!-- Screenfull -->
    <script src="assets/lib/screenfull/screenfull.js"></script>
    <script src="assets/lib/validationEngine/js/jquery.validationEngine.js"></script>
    <script src="assets/lib/validationEngine/js/languages/jquery.validationEngine-en.js"></script>
    <script src="assets/lib/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="assets/lib/jquery-validation/src/localization/messages_ja.js"></script>

    <!-- Metis core scripts -->
    <script src="assets/js/core.js"></script>

    <!-- Metis demo scripts -->
    <script src="assets/js/app.min.js"></script>
    <script>
      $(function() {
        Metis.formValidation();
      });
    </script>
    <script src="assets/js/style-switcher.js"></script>
    
	<!-- DATAPICKER -->
    <script src="assets/lib/jquery.uniform/jquery.uniform.min.js"></script>
    <script src="assets/lib/inputlimiter/jquery.inputlimiter.js"></script>
    <script src="assets/lib/chosen/chosen.jquery.min.js"></script>
    <script src="assets/lib/colorpicker/js/bootstrap-colorpicker.js"></script>
    <script src="assets/lib/tagsinput/jquery.tagsinput.js"></script>
    <script src="assets/lib/validVal/js/jquery.validVal.min.js"></script>
    <script src="assets/lib/datepicker/js/bootstrap-datepicker.js"></script>
    <script>
      $(function() {
        Metis.formGeneral();
      });
    </script>