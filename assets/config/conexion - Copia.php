﻿<?php
class Conexion{
	private $servidor;
	private $usuario;
	private $clave;
	private $basedatos;
	private $id;
			
	function __construct(){
		if ($_SERVER['HTTP_HOST']=='localhost'){
			$this->servidor	= 'localhost';
			$this->usuario	= 'root';
			$this->clave	= '';
			$this->basedatos= 'inspeccion';
		}else{
			$this->servidor	= 'localhost';
			$this->usuario	= '';
			$this->clave	= '';
			$this->basedatos= '';
		}
		
		$this->id=@mysql_connect($this->servidor,$this->usuario,$this->clave);
		if($this->id===false){
			die('ERROR: No pudo conectarse: <br />'.mysql_error());
		}

		$bd=@mysql_select_db($this->basedatos,$this->id);
		if($bd===false){
			die('ERROR: No pudo conectarse: <br />'.mysql_error());
		}
		
		mysql_query("SET NAMES UTF8");		
		return true;
	}
	
	function ejecutar($query){
		mysql_query("SET NAMES 'utf8'");
		$resultado=@mysql_query($query,$this->id);
		if($resultado===false){
			die("<b>ERROR en Sentencia:</b> ".mysql_error()."</br>".$query);
		}
		return $resultado;
	}

	function cantidad($recurso){
		$cantidad=@mysql_num_rows($recurso);
		if($cantidad===false){
			die("<b>ERROR en Sentencia:</b> ".mysql_error()."</br>".$query);
		}		
		return $cantidad;
	}
	
	function Arreglo($recurso,$tipo){
		$arreglo=mysql_fetch_array($recurso,$tipo);
		return $arreglo;
	}
	
	function Elemento($recurso,$fila,$campo){
		$elemento=mysql_result($recurso,$fila,$campo);
		return $elemento;
	}

    function ruta(){
        if ($_SERVER['HTTP_HOST']=='localhost'){
	        return 'http://'.$_SERVER['HTTP_HOST'].'/CNAE/inspeccion/';
		}else{
	        return 'http://'.$_SERVER['HTTP_HOST'].'/';			
		}
    }
	
    function rutAbsoluta(){
		if ($_SERVER['HTTP_HOST']=='localhost'){		
	        return $_SERVER['DOCUMENT_ROOT'].'/CNAE/inspeccion/';
		}else{
	        return $_SERVER['DOCUMENT_ROOT'].'/';			
		}
    }	

	///// CONVIERTE FECHA 1980-07-04 A 04/07/1980 (FORMATO ESPANOL)
	function FechaNOSQL($FE_Fecha)
	{
		$partes = explode("-", $FE_Fecha);
		$FE_FechaNac = $partes[2].'-'.$partes[1].'-'.$partes[0];
		return $FE_FechaNac;
	}
	
	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO SQL)
	function FechaSQL($FE_Fecha)
	{
		$partes = explode("-", $FE_Fecha);
		$FE_FechaNac = $partes[2].'-'.$partes[1].'-'.$partes[0];
		return $FE_Fecha;
	}	
	///////////// CONVERTIR DECIMALES A ESPANOL: 10.52 A 10,52 ///////////
	function DecimalEsp($numero){
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}

	///////////// CONVERTIR DECIMALES A AMERICANO 1.520,21 A 1520.21 ///////////	
	function DecimalAme($numero){
		$numero = str_replace(".", "", $numero);
		$numero = str_replace(",", ".", $numero);
		return $numero;
	}	
	///// CALCULAR EDAD DE LA FECHA DE NACIMIENTO
	function CalcEdad($FE_Fecha)
	{

		list($ano_nac,$mes_nac,$dia_nac) = explode("-", $FE_Fecha);
		$cumple = mktime(0, 0, 0, $mes_nac, $dia_nac, $ano_nac);
		$ahora	= time();
		$edad	= $ahora - $cumple;
		$anos	= $edad/60/60/24/365;
		$a_resto= $anos-(int)$anos;
		$meses	= $a_resto*12;
		$m_resto= $meses-(int)$meses;
		$dias	= (int)($m_resto*30);
		
		$anos	= (int)$anos;
		if ($anos>1){
			$titulo1 = 'Años';
		}else{
			$titulo1 = 'Año';			
		}
		$meses	= (int)$meses;
		if ($meses>1){
			$titulo2 = 'Meses';					
		}else{
			$titulo2 = 'Mes';								
		}
		
		$edad = $anos.' '.$titulo1.' <br/> '.$meses.' '.$titulo2; 
		return $edad;
	}	
	
	function __destruct(){
		mysql_close($this->id);
	}
}
?>