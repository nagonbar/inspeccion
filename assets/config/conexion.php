﻿<?php
class Conexion{
	private $servidor;
	private $usuario;
	private $clave;
	private $basedatos;
	private $id;
		
	function __construct(){
		$this->servidor	= 'localhost';
		$this->usuario	= 'root';
		$this->clave	= '';
		$this->basedatos= 'inspeccion';
		
		$this->link = mysqli_connect($this->servidor, $this->usuario, $this->clave, $this->basedatos);
		
		/* comprobar la conexiÃ³n */
		if (mysqli_connect_errno()) {
			printf("FallÃ³ la conexiÃ³n: %s\n", mysqli_connect_error());
			exit();
		}
		
		mysqli_set_charset($this->link, "utf8");
		return true;
	}
	
	function ejecutar($query){
		$resultado = mysqli_query($this->link, $query);
		if($resultado===false){
			die("<b>ERROR en Sentencia:</b></br></br>".mysqli_error($this->link)."</br></br>".$query);
		}
		return $resultado;
	}

	function cantidad($consulta){
		$cantidad = mysqli_num_rows($consulta);

		if($cantidad===false){
			die("<b>ERROR en Sentencia:</b></br></br>".mysqli_error($this->link)."</br></br>".$query);
		}		
		return $cantidad;
	}
	
	function Arreglo($consulta,$nro){

		$arreglo = mysqli_fetch_array($consulta, MYSQLI_NUM);
		return $arreglo[$nro];
	}
	
	function Elemento($consulta,$fila,$campo){
		mysqli_data_seek($consulta, $fila);
		$elemento = mysqli_fetch_array($consulta, MYSQLI_ASSOC);
		return $elemento[$campo];
	}

    function ruta(){
        if ($_SERVER['HTTP_HOST']=='localhost'){
	        return 'http://'.$_SERVER['HTTP_HOST'].'/CNAE/inspeccion/';
		}else{
	        return 'http://'.$_SERVER['HTTP_HOST'].'/';			
		}
    }
	
    function rutAbsoluta(){
		if ($_SERVER['HTTP_HOST']=='localhost'){		
	        return $_SERVER['DOCUMENT_ROOT'].'/CNAE/inspeccion/';
		}else{
	        return $_SERVER['DOCUMENT_ROOT'].'/';			
		}
    }	

	///// CONVIERTE FECHA 1980-07-04 A 04/07/1980 (FORMATO ESPANOL)
	function FechaNOSQL($FE_Fecha)
	{
		$partes = explode("-", $FE_Fecha);
		$FE_FechaNac = $partes[2].'-'.$partes[1].'-'.$partes[0];
		return $FE_FechaNac;
	}
	
	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO SQL)
	function FechaSQL($FE_Fecha)
	{
		$partes = explode("-", $FE_Fecha);
		$FE_FechaNac = $partes[2].'-'.$partes[1].'-'.$partes[0];
		return $FE_Fecha;
	}	
	///////////// CONVERTIR DECIMALES A ESPANOL: 10.52 A 10,52 ///////////
	function DecimalEsp($numero){
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}

	///////////// CONVERTIR DECIMALES A AMERICANO 1.520,21 A 1520.21 ///////////	
	function DecimalAme($numero){
		$numero = str_replace(".", "", $numero);
		$numero = str_replace(",", ".", $numero);
		return $numero;
	}	
	///// CALCULAR EDAD DE LA FECHA DE NACIMIENTO
	function CalcEdad($FE_Fecha)
	{

		list($ano_nac,$mes_nac,$dia_nac) = explode("-", $FE_Fecha);
		$cumple = mktime(0, 0, 0, $mes_nac, $dia_nac, $ano_nac);
		$ahora	= time();
		$edad	= $ahora - $cumple;
		$anos	= $edad/60/60/24/365;
		$a_resto= $anos-(int)$anos;
		$meses	= $a_resto*12;
		$m_resto= $meses-(int)$meses;
		$dias	= (int)($m_resto*30);
		
		$anos	= (int)$anos;
		if ($anos>1){
			$titulo1 = 'Años';
		}else{
			$titulo1 = 'Año';			
		}
		$meses	= (int)$meses;
		if ($meses>1){
			$titulo2 = 'Meses';					
		}else{
			$titulo2 = 'Mes';								
		}
		
		$edad = $anos.' '.$titulo1.' <br/> '.$meses.' '.$titulo2; 
		return $edad;
	}	
	
	function __destruct(){
		mysqli_close($this->link);
	}
}
?>