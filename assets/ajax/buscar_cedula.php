<?php
	require_once('../config/conexion.php');
	require_once('../model/m_usuario.php');
	
	$objConexion 		= new Conexion();

	if(!empty($_POST['cedula'])){
		$objUsuario	= new Usuario();

		$RSUsuario	= $objUsuario->BuscarXcedula($objConexion,$_POST['cedula']);
		$cRSUsuario	= $objConexion->cantidad($RSUsuario);
	
		if ($cRSUsuario>0){
			
			echo '<div align="center">Este Usuario ya esta registrado.</div>';
			
		}else{
			echo '    <div class="form-group">
      <label for="AL_Nombre" class="control-label col-lg-4">Nombre:</label>
        <div class="col-lg-8">
       	  <input class="form-control top validate[required]" type="text" id="AL_Nombre" name="AL_Nombre"/>
        </div>           
    </div><!-- /.form-group -->
        
    <div class="form-group">
      <label for="AL_Apellido" class="control-label col-lg-4">Apellido:</label>
        <div class="col-lg-8">
       	  <input class="form-control top validate[required]" type="text" id="AL_Apellido" name="AL_Apellido"/>
        </div>
    </div><!-- /.form-group -->    

    <div class="form-group">
      <label for="AF_Correo" class="control-label col-lg-4">Correo:</label>
        <div class="col-lg-8">
       	  <input class="form-control top validate[required,custom[email]]" type="text" id="AF_Correo" name="AF_Correo"/>
        </div>
    </div><!-- /.form-group -->    

    <div class="form-group">
      <label for="AF_Telefono" class="control-label col-lg-4">Telefono:</label>
        <div class="col-lg-8">
       	  <input class="form-control" type="tel" id="AF_Telefono" name="AF_Telefono"/>
        </div>
    </div><!-- /.form-group -->  

    <div class="form-group">
      <label for="AF_Clave" class="control-label col-lg-4">Clave:</label>
        <div class="col-lg-8">
       	  <input class="form-control top validate[required]" type="password" id="AF_Clave" name="AF_Clave"/>
        </div>
    </div><!-- /.form-group -->    

    <div class="form-group">
      <label for="AF_Clave2" class="control-label col-lg-4">Repetir Clave:</label>
        <div class="col-lg-8">
       	  <input class="form-control top validate[required, equals[AF_Clave]]" type="password" id="AF_Clave2" name="AF_Clave2"/>
        </div>
    </div><!-- /.form-group -->  

  <div class="form-group">
        <label for="AF_Telefono" class="control-label col-lg-4">&nbsp;</label>
        <div class="col-lg-8">
            <input type="submit" id="enviar" name="enviar" class="btn btn-danger btn-grad" value="Agregar">
            <a href="javascript:history.go(-1)" class="btn btn-metis-5 btn-grad" data-original-title="" title="">Cancelar</a>
        </div>
  </div>
	';	
		}
	}
?>