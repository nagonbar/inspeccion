<?php 
class Modulo{
	
	function Listar($objConexion){

		$query="SELECT *
				FROM modulo
				ORDER BY NU_Orden ASC";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function RegistroModulo($objConexion,$AF_NombreModulo,$AF_Ruta,$NU_Orden){
						
		$query="INSERT INTO modulo 
					(AF_NombreModulo, AF_Ruta, NU_Orden)
				VALUES
					('".$AF_NombreModulo."', '".$AF_Ruta."', '".$NU_Orden."');";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function BuscarXid($objConexion, $NU_IdModulo){

		$query="SELECT U.*
				FROM modulo AS U
				WHERE NU_IdModulo='".$NU_IdModulo."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}	
						
	function EditarModulo($objConexion,$NU_IdModulo,$AF_NombreModulo,$AF_Ruta,$NU_Orden){
						
		$query="UPDATE modulo SET
					AF_NombreModulo='".$AF_NombreModulo."', AF_Ruta='".$AF_Ruta."', NU_Orden='".$NU_Orden."'
				WHERE NU_IdModulo='".$NU_IdModulo."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function EliminarModulo($objConexion,$NU_IdModulo){
						
		$query="DELETE FROM modulo
				WHERE NU_IdModulo='".$NU_IdModulo."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}	
}
?>