<?php 
class Usuario{
	
	function autenticarUsuario($objConexion,$NU_Cedula,$AF_Clave){
		$this->NU_Cedula 	= $NU_Cedula;
		$this->AF_Clave		= $AF_Clave;
		$query="SELECT U.*
				FROM usuario AS U
				WHERE U.BI_Activo = 1 AND NU_Cedula = '".$this->NU_Cedula."' AND AF_Clave = '".$this->AF_Clave."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}

	function ultimoAcceso($objConexion,$NU_IdUsuario){
		$this->NU_IdUsuario = $NU_IdUsuario;
		$query="UPDATE usuario SET
				FE_UltimoAcceso = '".date('Y-m-d H:i')."'
				WHERE NU_IdUsuario = ".$this->NU_IdUsuario;
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function BuscarCorreo($objConexion,$AF_Correo){
		$this->AF_Correo 	= $AF_Correo;

		$query="SELECT U.*
				FROM usuario AS U
				WHERE U.BI_Activo = 1 AND AF_Correo = '".$this->AF_Correo."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function NuevaClave($objConexion,$NU_IdUsuario){
		$this->NU_IdUsuario = $NU_IdUsuario;
		$cadena = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789";
		$longitudCadena=strlen($cadena);
		$AF_Clave = "";
		$longitudPass=6;
		for($i=1 ; $i<=$longitudPass ; $i++){
			$pos = rand(0,$longitudCadena-1);
			$AF_Clave .= substr($cadena,$pos,1);
		}
		
		$this->AF_Clave	= md5($AF_Clave);

		$query="UPDATE usuario SET
				AF_Clave='".$this->AF_Clave."'
				WHERE NU_IdUsuario=".$this->NU_IdUsuario;

		$resultado=$objConexion->ejecutar($query);
		
		return $AF_Clave;
	}		

	function RegistroUsuario($objConexion,$NU_Cedula,$AL_Nombre,$AL_Apellido,$FE_FechaNac,$AF_Clave,$AF_Correo,$AF_Telefono){
						
		$query="INSERT INTO usuario 
					(rol_NU_IdRol, NU_Cedula, AL_Nombre, AL_Apellido, FE_FechaNac, AF_Clave, AF_Correo, AF_Telefono, BI_Admin, BI_Activo)
				VALUES
					(1, ".$NU_Cedula.", '".$AL_Nombre."', '".$AL_Apellido."', '".$FE_FechaNac."', '".$FE_FechaNac."', '".$AF_Correo."', '".$AF_Telefono."', 0, 1);
";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function ResetClave($objConexion,$NU_IdUsuario,$NU_Cedula){

		$this->AF_Clave	= md5($NU_Cedula);

		$query="UPDATE usuario SET
				AF_Clave='".$this->AF_Clave."'
				WHERE NU_IdUsuario=".$NU_IdUsuario;

		$resultado=$objConexion->ejecutar($query);
		
		return $AF_Clave;
	}
	
	function Listar($objConexion){

		$query="SELECT U.*
				FROM usuario AS U
				WHERE U.BI_Activo = 1";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}

	function EditarUsuario($objConexion,$NU_IdUsuario,$AL_Nombre,$AL_Apellido,$AF_Correo,$AF_Telefono){
						
		$query="UPDATE usuario SET
					AL_Nombre='".$AL_Nombre."', AL_Apellido='".$AL_Apellido."', AF_Correo='".$AF_Correo."', AF_Telefono='".$AF_Telefono."'
				WHERE NU_IdUsuario='".$NU_IdUsuario."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function DesactivarUsuario($objConexion,$NU_IdUsuario){
						
		$query="UPDATE usuario SET
					BI_Activo='0'
				WHERE NU_IdUsuario='".$NU_IdUsuario."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}	
	
	function BuscarXcedula($objConexion, $NU_Cedula){

		$query="SELECT U.*
				FROM usuario AS U
				WHERE U.BI_Activo = 1 and NU_Cedula='".$NU_Cedula."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}		
	
	function BuscarXid($objConexion, $NU_IdUsuario){

		$query="SELECT U.*
				FROM usuario AS U
				WHERE U.BI_Activo = 1 and NU_IdUsuario='".$NU_IdUsuario."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}			

	function ActAdmin($objConexion,$NU_IdUsuario,$BI_Admin){

		$query="UPDATE usuario SET
				BI_Admin = '".$BI_Admin."'
				WHERE NU_IdUsuario = '".$NU_IdUsuario."'";
				
	
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
}
?>