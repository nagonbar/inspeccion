<?php 
class Plantel{
	
	function BuscarXcod_cnae($objConexion,$cod_cnae){

		$query="SELECT P.*, U.nombre AS D_Nombre, U.apellido AS D_Apellido, U.telefono AS D_telefono, U.telefono_celular AS D_celular, U.email AS D_correo, SD.nombre AS SD_nombre, SD.apellido AS SD_apellido, SD.telefono AS SD_telefono, P.estado_id, E.nombre AS Estado, P.municipio_id,M.nombre AS Municipio, P.parroquia_id, PQ.nombre AS Parroquia
				FROM bd_plantel AS P
				LEFT JOIN bd_seguridad_usergroups_user AS U ON (P.director_actual_id=U.id)
				LEFT JOIN bd_seguridad_usergroups_user AS SD ON (P.subdirector_actual_id=SD.id)
				LEFT JOIN bd_estado AS E ON (P.estado_id=E.id)
				LEFT JOIN bd_municipio AS M ON (P.municipio_id=M.id)
				LEFT JOIN bd_parroquia AS PQ ON (P.parroquia_id=PQ.id)
				WHERE md5(cod_cnae)='".$cod_cnae."'";
		
		$resultado = $objConexion->ejecutar($query);
		return $resultado;		
	}
}
?>