<?php 
	require_once('../controller/c_session.php'); 
	require_once("../config/conexion.php"); 
	require_once("../model/m_privilegio.php"); 
	require_once("../model/m_usuario.php");	
	
	$objConexion 	= new Conexion();
	$objPrivilegio	= new Privilegio();
	$objUsuario		= new Usuario();
	
	$ruta = $objConexion->ruta();
	
	$RSTemas = $objPrivilegio->ListarModulosXidUser($objConexion,$_SESSION['NU_IdUsuario']);
	$cRSTemas= $objConexion->cantidad($RSTemas);
	
?>

<!doctype html>
<html class="no-js" lang="es">
  <head>
    <meta charset="UTF-8">
    <title><?php echo $_SESSION['NombreSistema']; ?></title>

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.min.css">
    <link rel="shortcut icon" href="../img/favicon.ico"/>        

    <!-- Font Awesome -->
    <link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="../css/main.min.css">

    <!-- Metis Theme stylesheet -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="assets/lib/html5shiv/html5shiv.js"></script>
        <script src="assets/lib/respond/respond.min.js"></script>
        <![endif]-->

    <!--For Development Only. Not required -->
    <script>
      less = {
        env: "development",
        relativeUrls: false,
        rootpath: "../assets/"
      };
    </script>
    <link rel="stylesheet" href="../css/style-switcher.css">
    <link rel="stylesheet/less" type="text/css" href="../css/less/theme.less">
    <script src="../lib/less/less-1.7.3.min.js"></script>

    <!--Modernizr 2.8.2-->
    <script src="../lib/modernizr/modernizr.min.js"></script>
    <script src="../js/session.js"></script>
<script language="JavaScript">
<!--
function resize_iframe()
{

	var height=window.innerWidth;//Firefox
	if (document.body.clientHeight)
	{
		height=document.body.clientHeight;//IE
	}
	
	var navegador = navigator.userAgent;
	if (navigator.userAgent.indexOf('MSIE') !=-1) {
		//IE
		document.getElementById("central").style.height=parseInt(height-document.getElementById("central").offsetTop+170)+"px";
	} else if (navigator.userAgent.indexOf('Firefox') !=-1) {
		//FIREFOX
		document.getElementById("central").style.height=parseInt(height-document.getElementById("central").offsetTop-70)+"px";
	} else if (navigator.userAgent.indexOf('Chrome') !=-1) {
		document.getElementById("central").style.height=parseInt(height-document.getElementById("central").offsetTop+120)+"px";
	} else if (navigator.userAgent.indexOf('Opera') !=-1) {
		document.getElementById("central").style.height=parseInt(height-document.getElementById("central").offsetTop-70)+"px";
	} else {
		document.getElementById("central").style.height=parseInt(height-document.getElementById("central").offsetTop-70)+"px";
	}
  
	
	
	//document.getElementById("releno").style.height=parseInt(height-document.getElementById("releno").offsetTop-850)+"px";
}


window.onresize=resize_iframe; 

</script>
  </head>
  <body onload="ini();" onkeypress="parar();" onclick="parar();" class="  ">
    <div class="bg-green dk" id="wrap">
      <div id="top">

        <!-- .navbar -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">

            <!-- Brand and toggle get grouped for better mobile display -->
            <header class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
              </button>
              <a href="index.php" class="navbar-brand">
                      <div class="form-signin" align="center" style="width:248px; background-color:#fff">
                      <img src="../img/logo.jpg" width="122" height="103"  alt=""/>
                      </div>
              </a> 
            </header>
            <div class="topnav">
               <div class="btn-group">
                <a data-placement="bottom" data-original-title="Barra Izquierda" data-toggle="tooltip" class="btn btn-primary btn-sm toggle-left" id="menu-toggle">
                  <i class="glyphicon glyphicon-th"></i>
                </a> 
                
                <?php /*?><a data-placement="bottom" data-original-title="Barra Derecha" data-toggle="tooltip" class="btn btn-default btn-sm toggle-right"> <span class="glyphicon glyphicon-comment"></span>  </a> <?php */?>
              
              </div>
              
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="Pantalla Completa" data-toggle="tooltip" class="btn btn-default btn-sm" id="toggleFullScreen">
                  <i class="glyphicon glyphicon-fullscreen"></i>
                </a> 
              </div>
              
              <div class="btn-group">
              <?php /*?>
                <a data-placement="bottom" data-original-title="E-mail" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-envelope"></i>
                  <span class="label label-warning">5</span> 
                </a> 
                <a data-placement="bottom" data-original-title="Messages" href="#" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-comments"></i>
                  <span class="label label-danger">4</span> 
                </a> 
			<?php */?>
                <a data-placement="bottom" data-original-title="Ayuda" data-toggle="modal" class="btn btn-default btn-sm" href="#helpModal">
                  <i class="glyphicon glyphicon-question-sign"></i>
                </a> 
              </div>
              <div class="btn-group">
                <a href="salir.php" data-toggle="tooltip" data-original-title="Salir" data-placement="bottom" class="btn btn-metis-1 btn-sm">
                  <i class="glyphicon glyphicon-off"></i>
                </a> 
              </div>

            </div>
			<?php 
			$RSUsuario = $objUsuario->BuscarXid($objConexion,$_SESSION['NU_IdUsuario']); 
			$cRSUsuario= $objConexion->cantidad($RSUsuario);
			
			if ($cRSUsuario>0){
				$BI_Admin = $objConexion->Elemento($RSUsuario,0,'BI_Admin');	
			}
			if($BI_Admin==='1'){
			?>            
            <div class="collapse navbar-collapse navbar-ex1-collapse">

              <!-- .nav -->
              <ul class="nav navbar-nav">
              
                <li class='dropdown '>
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Sistema
                    <b class="caret"></b>
                  </a> 
                  <ul class="dropdown-menu">
		              <!-- *********** MENU DE SEGURIDAD ***************  -->                  
                    <li> <a href="<?php echo $ruta; ?>assets/views/modulo/index.php" target="central">Gestión de Módulos</a>  </li>
                    <li> <a href="<?php echo $ruta; ?>assets/views/usuario/index.php" target="central">Gestión de Usuarios</a>  </li>                    
		              <!-- *************************************************  -->                                      
                  </ul>
                </li>

              </ul><!-- /.nav -->
              
            </div>
			<?php } ?>
          </div><!-- /.container-fluid -->
        </nav><!-- /.navbar -->
        <header class="head">
          
          <div class="search-bar">
<!-- /.main-search -->
          </div><!-- /.search-bar -->
          
          <div class="main-bar">
            <h3>
              <i class="glyphicon glyphicon-home"></i>&nbsp; <a href="central.php" target="central">Inicio</a></h3>
          </div><!-- /.main-bar -->
        </header><!-- /.head -->
      </div><!-- /#top -->
      <div id="left">
        <div class="media user-media bg-dark dker">
          <div class="user-media-toggleHover">
            <span class="glyphicon glyphicon-user"></span> 
          </div>
          <div class="user-wrapper bg-dark">
            <a class="user-link" href="">
              <img class="media-object img-thumbnail user-img" alt="User Picture" src="../img/user.gif">
              <?php /*?><span class="label label-danger user-label">16</span> <?php */?>
            </a> 
            <div class="media-body">
              <h5 class="media-heading">Usuario:</h5>
              <ul class="list-unstyled user-info">
                <li> <a href=""><?php echo ucwords(strtolower($_SESSION['AL_Nombre'].' '.$_SESSION['AL_Apellido'])); ?></a>  </li>
                <li>Último Acceso:
                  <br>
                  <small>
                    <i class="glyphicon glyphicon-calendar"></i>&nbsp;<?php echo date('d/m/Y - h:i a',strtotime($_SESSION['FE_UltimoAcceso'])); ?></small> 
                </li>
              </ul>
            </div><br>
          </div>
        </div>

        <!-- #menu -->
        <ul id="menu" class="bg-blue dker">
          <li class="nav-header">Menu</li>
          <li class="nav-divider"></li>
          <?php 
		  		if($cRSTemas>0){ 
		  			for($i=0; $i<$cRSTemas; $i++){
						$AF_NombreModulo 	= $objConexion->Elemento($RSTemas,$i,'AF_NombreModulo');
						$AF_Ruta 			= $ruta.'assets/views/'.$objConexion->Elemento($RSTemas,$i,'AF_Ruta');

						$nro = $i+1;
						
						echo '
							  <li class="">
								<a href="'.$AF_Ruta.'" target="central">
								  <img src="../img/botones/'.$nro.'.png" width="25" />
								  <span class="link-title">&nbsp;'.$AF_NombreModulo.'</span> 
								</a> 
							  </li>
						';

					}
		  		}
		  ?>

          <li class="nav-divider"></li>
          <li>
            <a href="salir.php">
              <img src="../img/botones/salir.png" width="25" />
              <span class="link-title">
    Salir
    </span> 
            </a> 
          </li>

        </ul><!-- /#menu -->
      </div><!-- /#left -->
      <div id="content">
        <div class="outer">
          <div class="inner bg-light lter">
            <div class="col-lg-12">

  				<iframe width="100%" height="740px" src="central.php" vspace="0" scrolling="yes" frameborder="0" id="central" name="central" ></iframe>

            </div>
          </div><!-- /.inner -->
        </div><!-- /.outer -->
      </div><!-- /#content -->
      
    </div><!-- /#wrap -->
    <footer class="Footer bg-dark dker">
      <p>2015 &copy; Sistema de Inspecciones</p>
    </footer><!-- /#footer -->

    <!-- #helpModal -->
    <div id="helpModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Ayuda</h4>
          </div>
          <div class="modal-body">
            <p>
              En Construcción.
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --><!-- /#helpModal -->

    <!--jQuery 2.1.1 -->
    <script src="../lib/jquery/jquery.min.js"></script>

    <!--Bootstrap -->
    <script src="../lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Screenfull -->
    <script src="../lib/screenfull/screenfull.js"></script>

    <!-- Metis core scripts -->
    <script src="../js/core.js"></script>

    <!-- Metis demo scripts -->
    <script src="../js/app.min.js"></script>
    <script src="../js/style-switcher.js"></script>
  </body>
</html>