<?php 
	session_start();
	
	require_once("../config/conexion.php"); 
	include_once("../model/m_usuario.php"); 	

	$objConexion	= new Conexion();
	$objUsuario		= new Usuario();
	
	if ($_SESSION['NU_IdUsuario']!=''){
		$objUsuario->ultimoAcceso($objConexion,$_SESSION['NU_IdUsuario']);
	}

	$objConexion->__destruct();
		
	$_SESSION = array();
	session_destroy();
			
	if($_GET['error']==4){
		$error = md5(4);
		header("Location: ../../index.php?error=".$error);	
	}else{
		header("Location: ../../index.php");	
	}
?>