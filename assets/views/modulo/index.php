<?php 
	require_once('../../controller/c_session.php'); 
	require_once("../../config/conexion.php"); 
	require_once("../../model/m_modulo.php");
	
	$objConexion 	= new Conexion();
	$objModulo		= new Modulo();
?>

<?php
	$RSModulo		= $objModulo->Listar($objConexion);
	$cRSModulo	= $objConexion->cantidad($RSModulo);
?>

<!doctype html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <title></title>

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="../../css/main.min.css">

    <!-- Metis Theme stylesheet -->
    <link rel="stylesheet" href="../../lib/datatables/3/dataTables.bootstrap.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="assets/lib/html5shiv/html5shiv.js"></script>
        <script src="assets/lib/respond/respond.min.js"></script>
        <![endif]-->

    <!--For Development Only. Not required -->
    <script>
      less = {
        env: "development",
        relativeUrls: false,
        rootpath: "../assets/"
      };
    </script>
    <link rel="stylesheet" href="../../css/style-switcher.css">
    <link rel="stylesheet/less" type="text/css" href="../../css/less/theme.less">
    <script src="../../lib/less/less-1.7.3.min.js"></script>


    <!--Modernizr 2.8.2-->
    <script src="../../lib/modernizr/modernizr.min.js"></script>
  </head>
  <body class="  ">
	<?php 
        if (isset($_GET['m'])){ 
			
            switch ($_GET['m']){
                case md5(0):
                    echo '<br><div class="alert alert-danger col-lg-12" align="center"><b>ERROR: Los datos de este Modulo estan incompletos.</b></div>';
                    break;
                case md5(1):
                    echo '<br><div class="alert alert-success col-lg-12" align="center"><b>Módulo registrado satisfactoriamente.</b></div>';
                    break;	
                case md5(2):
                    echo '<br><div class="alert alert-success col-lg-12" align="center"><b>Modulo editado correctamente.</b></div>';
                    break;	
                case md5(3):
                    echo '<br><div class="alert alert-warning col-lg-12" align="center"><b>Modulo eliminado!.</b></div>';
                    break;																		
            }
        }
    ?> 
            <!--Begin Datatables-->
            <div class="row">
              <div class="col-lg-12">
                <div class="box">
                  <header>
                    <div class="icons">
                      <i class="fa fa-table"></i>
                    </div>
                    <h5>Gestión de Módulos</h5>
                  </header>
                  <div id="collapse4" class="body">
                  <div align="center">
                  <a href="add.php" class="btn btn-danger btn-grad" data-original-title="" title="">
                      <i class="fa fa-plus-square"></i>
                      Crear Nuevo Módulo
                  </a>
                  </div>
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th>IdMódulo</th>
                          <th>Nombre del Modulo</th>
                          <th>Ruta</th>
                          <th>Orden</th>
                          <th width="50" align="center">Editar</th>
                          <th width="50" align="center">Borrar</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
					  if ($cRSModulo>0){ ?>
                      <?php
						  for ($i=0; $i<$cRSModulo; $i++){
							$NU_IdModulo 	= $objConexion->Elemento($RSModulo,$i,'NU_IdModulo');
							$AF_NombreModulo= $objConexion->Elemento($RSModulo,$i,'AF_NombreModulo');
							$AF_Ruta 		= $objConexion->Elemento($RSModulo,$i,'AF_Ruta');
							$NU_Orden 		= $objConexion->Elemento($RSModulo,$i,'NU_Orden');
						?>
						<tr>
                          <td>&nbsp;<?php echo $NU_IdModulo;?></td>
                          <td>&nbsp;<?php echo $AF_NombreModulo;?></td>
                          <td>&nbsp;<?php echo $AF_Ruta;?></td>
                          <td>&nbsp;<?php echo $NU_Orden;?></td>
                          <td align="center" valign="middle">
                          	<a href="edit.php?NU_IdModulo=<?php echo $NU_IdModulo; ?>">
                          		<i class="glyphicon glyphicon-edit"></i>
                            </a>
                          </td>
                          <td align="center" valign="middle">
                              <a href="del.php?NU_IdModulo=<?php echo $NU_IdModulo; ?>">
                                <i class="glyphicon glyphicon-trash"></i>
                              </a>
                          </td>
                        </tr>
								
					 <?php 
						 }
					  }
					   ?>

                       
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div><!-- /.row -->

            <!--End Datatables-->

    <!--jQuery 2.1.1 -->
    <script src="../../lib/jquery/jquery.min.js"></script>
    <script src="../../lib/jquery/jquery-ui.min.js"></script>

	<!--	MODAL-->
    <link rel="stylesheet" href="../../css/ventana_modal.css">
    <script src="../../js/jquery-1.7.2.min.js"></script>        
    <script src="../../js/ventana_modal.js"></script>

    <!--Bootstrap -->
    <script src="../../lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Screenfull -->
    <script src="../../lib/screenfull/screenfull.js"></script>
    <script src="../../lib/datatables/jquery.dataTables.js"></script>
    <script src="../../lib/datatables/3/dataTables.bootstrap.js"></script>
    <script src="../../lib/jquery.tablesorter/jquery.tablesorter.min.js"></script>
    <script src="../../lib/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

 


    
    <!-- Metis core scripts -->
    <script src="../../js/core.js"></script>

    <!-- Metis demo scripts -->
    <script src="../../js/app.min.js"></script>
    <script>
      $(function() {
        Metis.MetisTable();
        Metis.metisSortable();
      });
    </script>


  </body>
</html>