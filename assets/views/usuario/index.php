<?php 
	require_once('../../controller/c_session.php'); 
	require_once("../../config/conexion.php"); 
	require_once("../../model/m_usuario.php");
	
	$objConexion 	= new Conexion();
	$objUsuario		= new Usuario();
?>

<?php
	$RSUsuario	= $objUsuario->Listar($objConexion);
	$cRSUsuario	= $objConexion->cantidad($RSUsuario);
?>

<!doctype html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <title></title>

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="../../css/main.min.css">

    <!-- Metis Theme stylesheet -->
    <link rel="stylesheet" href="../../lib/datatables/3/dataTables.bootstrap.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="assets/lib/html5shiv/html5shiv.js"></script>
        <script src="assets/lib/respond/respond.min.js"></script>
        <![endif]-->

    <!--For Development Only. Not required -->
    <script>
      less = {
        env: "development",
        relativeUrls: false,
        rootpath: "../assets/"
      };
    </script>
    <link rel="stylesheet" href="../../css/style-switcher.css">
    <link rel="stylesheet/less" type="text/css" href="../../css/less/theme.less">
    <script src="../../lib/less/less-1.7.3.min.js"></script>


    <!--Modernizr 2.8.2-->
    <script src="../../lib/modernizr/modernizr.min.js"></script>
  </head>
  <body class="  ">
	<?php 
        if (isset($_GET['m'])){ 
			
            switch ($_GET['m']){
                case md5(0):
                    echo '<br><div class="alert alert-danger col-lg-12" align="center"><b>ERROR: Los datos de este Usuario estan incompletos.</b></div>';
                    break;
                case md5(1):
                    echo '<br><div class="alert alert-success col-lg-12" align="center"><b>Usuario registrad@ satisfactoriamente.</b></div>';
                    break;	
                case md5(2):
                    echo '<br><div class="alert alert-success col-lg-12" align="center"><b>La Clave del Usuario ha sido Reseteada. Debe usar su Nro. de Cedula como clave.</b></div>';
                    break;		
                case md5(3):
                    echo '<br><div class="alert alert-success col-lg-12" align="center"><b>Usuario editado correctamente.</b></div>';
                    break;	
                case md5(4):
                    echo '<br><div class="alert alert-warning col-lg-12" align="center"><b>Usuario eliminado!.</b></div>';
                    break;																		
                case md5(5):
                    echo '<br><div class="alert alert-success col-lg-12" align="center"><b>Privilegios Asignados con exito al Usuario correspondiente!.</b></div>';
                    break;																		

            }
        }
    ?> 
            <!--Begin Datatables-->
            <div class="row">
              <div class="col-lg-12">
                <div class="box">
                  <header>
                    <div class="icons">
                      <i class="fa fa-table"></i>
                    </div>
                    <h5>Gestión de Usuarios</h5>
                  </header>
                  <div id="collapse4" class="body">
                  <div align="center">
                  <a href="add.php" class="btn btn-danger btn-grad" data-original-title="" title="">
                      <i class="fa fa-plus-square"></i>
                      Crear Nuevo Usuario
                  </a>
                  </div>
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th>IdUsuario</th>
                          <th>Cedula</th>
                          <th>Nombre</th>
                          <th>Apellido</th>
                          <th>Correo</th>
                          <th>Último Acceso</th>
                          <th>Fecha Creación</th>
                          <th width="50" align="center">Privilegios</th>
                          <th width="50" align="center">Resetear</th> 
						  <th width="50" align="center">Editar</th>
                          <th width="50" align="center">Borrar</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
					  if ($cRSUsuario>0){ ?>
                      <?php
						  for ($i=0; $i<$cRSUsuario; $i++){
							$NU_IdUsuario 		= $objConexion->Elemento($RSUsuario,$i,'NU_IdUsuario');
							$NU_Cedula 			= $objConexion->Elemento($RSUsuario,$i,'NU_Cedula');
							$AL_Nombre 			= $objConexion->Elemento($RSUsuario,$i,'AL_Nombre');
							$AL_Apellido 		= $objConexion->Elemento($RSUsuario,$i,'AL_Apellido');
							$AF_Correo 			= $objConexion->Elemento($RSUsuario,$i,'AF_Correo');																					
							$FE_UltimoAcceso 	= $objConexion->Elemento($RSUsuario,$i,'FE_UltimoAcceso');
							$FE_Registro		= $objConexion->Elemento($RSUsuario,$i,'FE_Registro');
						?>
						<tr>
                          <td>&nbsp;<?php echo $NU_IdUsuario;?></td>
                          <td>&nbsp;<?php echo $NU_Cedula;?></td>
                          <td>&nbsp;<?php echo $AL_Nombre;?></td>
                          <td>&nbsp;<?php echo $AL_Apellido;?></td>
                          <td>&nbsp;<?php echo $AF_Correo;?></td>
                          <td>&nbsp;<?php echo $FE_UltimoAcceso;?></td>
                          <td>&nbsp;<?php echo $FE_Registro;?></td>
                          <td align="center" valign="middle">
                          	<a href="privilegio.php?NU_IdUsuario=<?php echo $NU_IdUsuario; ?>">
                            	<i class="glyphicon glyphicon-wrench"></i>
                            </a>
                          </td>
                          <td align="center" valign="middle">
                          	<a href="../../controller/c_usuario.php?origen=CambioClave&NU_IdUsuario=<?php echo $NU_IdUsuario; ?>">
                            	<i class="glyphicon glyphicon-refresh"></i>
                            </a>
                          </td>
                          <td align="center" valign="middle">
                          	<a href="edit.php?NU_IdUsuario=<?php echo $NU_IdUsuario; ?>">
                          		<i class="glyphicon glyphicon-edit"></i>
                            </a>
                          </td>
                          <td align="center" valign="middle">
                              <a href="del.php?NU_IdUsuario=<?php echo $NU_IdUsuario; ?>">
                                <i class="glyphicon glyphicon-trash"></i>
                              </a>
                          </td>
                        </tr>
								
					 <?php 
						 }
					  }
					   ?>

                       
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div><!-- /.row -->

            <!--End Datatables-->

    <!--jQuery 2.1.1 -->
    <script src="../../lib/jquery/jquery.min.js"></script>
    <script src="../../lib/jquery/jquery-ui.min.js"></script>

	<!--	MODAL-->
    <link rel="stylesheet" href="../../css/ventana_modal.css">
    <script src="../../js/jquery-1.7.2.min.js"></script>        
    <script src="../../js/ventana_modal.js"></script>

    <!--Bootstrap -->
    <script src="../../lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Screenfull -->
    <script src="../../lib/screenfull/screenfull.js"></script>
    <script src="../../lib/datatables/jquery.dataTables.js"></script>
    <script src="../../lib/datatables/3/dataTables.bootstrap.js"></script>
    <script src="../../lib/jquery.tablesorter/jquery.tablesorter.min.js"></script>
    <script src="../../lib/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

 


    
    <!-- Metis core scripts -->
    <script src="../../js/core.js"></script>

    <!-- Metis demo scripts -->
    <script src="../../js/app.min.js"></script>
    <script>
      $(function() {
        Metis.MetisTable();
        Metis.metisSortable();
      });
    </script>


  </body>
</html>