<?php 
	require_once('../../controller/c_session.php'); 
	require_once("../../config/conexion.php");
	require_once("../../model/m_usuario.php");	
	require_once("../../model/m_modulo.php");	
	require_once("../../model/m_privilegio.php");			
	
	$objConexion	= new Conexion();  
	$objUsuario		= new Usuario();
	$objModulo		= new Modulo();	
	$objPrivilegio	= new Privilegio();
			
	$ruta			= $objConexion->ruta();
	
	$NU_IdUsuario	= $_GET['NU_IdUsuario'];
	
	$RSUsuario	= $objUsuario->BuscarXid($objConexion,$NU_IdUsuario);
	$cRSUsuario = $objConexion->cantidad($RSUsuario);
	
	if ($cRSUsuario>0){
		$AL_Nombre 		= $objConexion->Elemento($RSUsuario,0,'AL_Nombre');	
		$AL_Apellido 	= $objConexion->Elemento($RSUsuario,0,'AL_Apellido');	
		$NU_Cedula 		= $objConexion->Elemento($RSUsuario,0,'NU_Cedula');	
		$BI_Admin 		= $objConexion->Elemento($RSUsuario,0,'BI_Admin');			
	}
	
	$RSTema	= $objModulo->Listar($objConexion);
	$cRSTema	= $objConexion->cantidad($RSTema);
	
?>

<!doctype html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <title>Form General</title>

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="../../css/main.min.css">

    <!-- Metis Theme stylesheet -->
    <link rel="stylesheet" href="../../lib/jquery.uniform/themes/default/css/uniform.default.css">
    <link rel="stylesheet" href="../../lib/inputlimiter/jquery.inputlimiter.css">
    <link rel="stylesheet" href="../../lib/chosen/chosen.min.css">
    <link rel="stylesheet" href="../../lib/colorpicker/css/colorpicker.css">
    <link rel="stylesheet" href="../../css/colorpicker_hack.css">
    <link rel="stylesheet" href="../../lib/tagsinput/jquery.tagsinput.css">
    <link rel="stylesheet" href="../../lib/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="../../lib/datepicker/css/datepicker.css">
    <link rel="stylesheet" href="../../lib/timepicker/css/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="../../lib/switch/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="../../lib/jasny-bootstrap/css/jasny-bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="assets/lib/html5shiv/html5shiv.js"></script>
        <script src="assets/lib/respond/respond.min.js"></script>
        <![endif]-->

    
    <!--For Development Only. Not required -->
    <script>
      less = {
        env: "development",
        relativeUrls: false,
        rootpath: "../assets/"
      };
    </script>
    <link rel="stylesheet/less" type="text/css" href="../../css/less/theme.less">
    <script src="../../lib/less/less-1.7.3.min.js"></script>

    <!--Modernizr 2.8.2-->
    <script src="../../lib/modernizr/modernizr.min.js"></script>
	

<br>

<div style="font-weight:bold; font-size:14px">
Gestión de Usuarios > Gestionar Privilegios</div>

<form class="form-horizontal" id="form1" name="form1" action="../../controller/c_privilegio.php" method="post" autocomplete="off" enctype="multipart/form-data">

<!--INICIO DATOS DE EQUIPO -->
<div class="row">
  <div class="col-lg-6">
    <div class="box dark">
      <header>

        <div class="icons">
          <i class="fa fa-edit"></i>
        </div>

        <h5>Privilegios del Usuario: </h5>

        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a> 
            <a href="javascript:;" class="btn btn-default btn-xs full-box">
              <i class="fa fa-expand"></i>
            </a> 
            <a href="javascript:;" class="btn btn-danger btn-xs close-box">
              <i class="fa fa-times"></i>
            </a> 
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div id="div-1" class="body">

   <div class="form-group">
        <label for="NU_Cedula" class="control-label col-lg-4">Cédula:</label>
        <div class="col-lg-8">
        	<?php echo $NU_Cedula; ?>
        </div>
    </div><!-- /.form-group -->

	<div class="form-group">
      <label for="AL_Nombre" class="control-label col-lg-4">Nombre:</label>
        <div class="col-lg-8">
       	  <?php echo $AL_Nombre.' '.$AL_Apellido; ?>
        </div>           
    </div><!-- /.form-group -->

<?php
	if ($BI_Admin=='1'){
		$check = 'checked="checked"';
	}else{
		$check = '';
	}
?>
	<div class="form-group">
      <label for="BI_Admin" class="control-label col-lg-4">Administrador:</label>
        <div class="col-lg-8">
       	   <input name="BI_Admin" type="checkbox" id="BI_Admin" value="1" <?php echo $check; ?>>
        </div>           
    </div><!-- /.form-group -->
            
        	</div>
        </div>
    </div>
</div>

<!--FIN DATOS DEL USUARIO -->
<!--INICIO PRIVILEGIOS -->

<div class="row">
  <div class="col-lg-6">
    <div class="box dark">
      <header>
        <div class="icons">
          <i class="fa fa-edit"></i>
        </div>
        <h5>Asigne el o los modulos correspondiente a este usuario: </h5>

        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a> 
            <a href="javascript:;" class="btn btn-default btn-xs full-box">
              <i class="fa fa-expand"></i>
            </a> 
            <a href="javascript:;" class="btn btn-danger btn-xs close-box">
              <i class="fa fa-times"></i>
            </a> 
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div id="div-1" class="body">
	<?php if ($cRSTema>0){ 
			for($i=0; $i<$cRSTema; $i++){
				$NU_IdModulo 		= $objConexion->Elemento($RSTema,$i,'NU_IdModulo');
				$AF_NombreModulo 	= $objConexion->Elemento($RSTema,$i,'AF_NombreModulo');
				
				$Privilegio			= $objPrivilegio->BuscarPrivilegiosXuser($objConexion,$NU_IdUsuario,$NU_IdModulo);
				
				if ($Privilegio>0){
					$check = 'checked="checked"';
				}else{
					$check = '';
				}
				
	?>
        <div class="form-group">
          <label for="mod<?php echo $i; ?>" class="control-label col-lg-4"><?php echo $AF_NombreModulo; ?></label>
            <div class="col-lg-8">
              <input name="mod<?php echo $i; ?>" type="checkbox" id="mod<?php echo $i; ?>" value="<?php echo $NU_IdModulo; ?>" <?php echo $check; ?>>
            </div>           
        </div><!-- /.form-group -->
	<?php } } ?>

    <div class="form-group">
        <label for="" class="control-label col-lg-4">&nbsp;</label>
        <div class="col-lg-8">
            <input type="submit" id="enviar" name="enviar" class="btn btn-danger btn-grad" value="Asignar">
            <a href="javascript:history.go(-1)" class="btn btn-metis-5 btn-grad" data-original-title="" title="">Cancelar</a>
        </div>
    </div>
        
    <input type="hidden" id="origen" name="origen" value="privilegioU">
	<input type="hidden" id="NU_IdUsuario" name="NU_IdUsuario" value="<?php echo $NU_IdUsuario; ?>">
 </form>
              <!--END TEXT INPUT FIELD-->

    <!--jQuery 2.1.1 -->
    <script src="../../lib/jquery/jquery.min.js"></script>

    <!--Bootstrap -->
    <script src="../../lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Screenfull -->
    <script src="../../lib/screenfull/screenfull.js"></script>
    <script src="../../lib/jquery.uniform/jquery.uniform.min.js"></script>
    <script src="../../lib/inputlimiter/jquery.inputlimiter.js"></script>
    <script src="../../lib/chosen/chosen.jquery.min.js"></script>
    <script src="../../lib/colorpicker/js/bootstrap-colorpicker.js"></script>
    <script src="../../lib/tagsinput/jquery.tagsinput.js"></script>
    <script src="../../lib/validVal/js/jquery.validVal.min.js"></script>
    <script src="../../lib/moment/moment.min.js"></script>
    <script src="../../lib/daterangepicker/daterangepicker.js"></script>
    <script src="../../lib/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../../lib/timepicker/js/bootstrap-timepicker.min.js"></script>
    <script src="../../lib/switch/js/bootstrap-switch.min.js"></script>
    <script src="../../lib/autosize/jquery.autosize.min.js"></script>
    <script src="../../lib/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
    <script src="../../js/jquery.maskMoney.js" type="text/javascript"></script>


    <!-- Metis core scripts -->
    <script src="../../js/core.js"></script>

    <!-- Metis demo scripts -->
    <script src="../../js/app.min.js"></script>
    <script>
      $(function() {
        Metis.formGeneral();
      });
    </script>

	<script type="text/javascript">
		$(document).ready(function(){
			$("#BuscarR1").click(function(e){
					 cedula = $("#NU_Cedula").val();
					 $("#datos_usuario").delay(1000).queue(function(n) {     
						  $("#datos_usuario").html('<div align="center"><img src="../../img/ajax-loader.gif" /></div>');
								$.ajax({
									  type: "POST",
									  url: "../../ajax/buscar_cedula.php",
									  data: "cedula="+cedula,
									  dataType: "html",
									  error: function(){
											alert("error petición ajax");
									  },
									  success: function(data){                                                     
											$("#datos_usuario").html(data);
											n();
									  }
						  });
					 });
			  });
		}); 
    </script>  


  </body>
</html>