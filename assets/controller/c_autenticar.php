<?php 
	if (isset($_POST['submit'])){
		require_once("../config/conexion.php"); 
		require_once("../model/m_usuario.php"); 	
	
		$NU_Cedula		= $_POST["NU_Cedula"];
		$AF_Clave 		= md5($_POST["AF_Clave"]);
	
		$objConexion	= new Conexion();
		$objUsuario 	= new Usuario();
		
		$ruta			= $objConexion->ruta();
		
		$RSUsuario		= $objUsuario->autenticarUsuario($objConexion,$NU_Cedula,$AF_Clave);
		$cRSUsuario		= $objConexion->cantidad($RSUsuario);
	
		if ($cRSUsuario != 0){
			
			session_start();
			
			$_SESSION['NombreSistema'] 	= 'inspeccion';
			$_SESSION['NU_IdUsuario'] 	= $objConexion->Elemento($RSUsuario,0,"NU_IdUsuario");
			$_SESSION['AL_Nombre'] 		= $objConexion->Elemento($RSUsuario,0,"AL_Nombre");
			$_SESSION['NU_IdEstado'] 		= $objConexion->Elemento($RSUsuario,0,"NU_IdEstado");
			$_SESSION['AL_Apellido'] 	= $objConexion->Elemento($RSUsuario,0,"AL_Apellido");
			$_SESSION['rol_NU_IdRol'] 	= $objConexion->Elemento($RSUsuario,0,"rol_NU_IdRol");	
			$_SESSION['BI_Admin'] 		= $objConexion->Elemento($RSUsuario,0,"BI_Admin");
			$_SESSION['FE_UltimoAcceso']= $objConexion->Elemento($RSUsuario,0,"FE_UltimoAcceso");
			
			header("Location: ".$ruta."assets/views/index.php");			
		}else{
			$error = md5('1');
			header("Location: ".$ruta."index.php?error=".$error);		
		}
	}
?>