<?php
	require_once("../config/conexion.php"); 
	require_once("../model/m_usuario.php");
	require_once("../model/m_modulo.php");	
	require_once("../model/m_privilegio.php");		
	
	if ($_POST['origen']=='privilegioU'){

		$objConexion	= new Conexion();  
		$objUsuario 	= new Usuario();
		$objModulo		= new Modulo();
		$objPrivilegio	= new Privilegio();
		
		$ruta			= $objConexion->ruta();
		
		$NU_IdUsuario 	= $_POST['NU_IdUsuario'];	
		
		$RSTema		= $objModulo->Listar($objConexion);
		$cRSTema		= $objConexion->cantidad($RSTema);

		if ($cRSTema>0){		
			for($i=0; $i<$cRSTema; $i++){
				$NU_IdModulo = $_POST['mod'.$i];
				if ($NU_IdModulo!=''){
					$RSBuscar = $objPrivilegio->BuscarPrivilegiosXuser($objConexion,$NU_IdUsuario,$NU_IdModulo);
					if($RSBuscar===0){
						$objPrivilegio->AsignarPrivilegio($objConexion,$NU_IdUsuario,$NU_IdModulo);
					}
				}else{
					$NU_IdModulo= $objConexion->Elemento($RSTema,$i,'NU_IdModulo');
					$RSBuscar 	= $objPrivilegio->BuscarPrivilegiosXuser($objConexion,$NU_IdUsuario,$NU_IdModulo);
					if($RSBuscar>0){
						$objPrivilegio->EliminarPrivilegio($objConexion,$NU_IdUsuario,$NU_IdModulo);
					}
				}
			}
		}

		$RSUsuario 		= $objUsuario->BuscarXid($objConexion,$NU_IdUsuario);
		$BI_AdminOri 	= $objConexion->Elemento($RSUsuario,0,'BI_Admin');

		if ($_POST['BI_Admin']=='1'){
			if($BI_AdminOri==='0'){
				$objUsuario->ActAdmin($objConexion,$NU_IdUsuario,1);
			}
		}else{
			if($BI_AdminOri==='1'){
				$objUsuario->ActAdmin($objConexion,$NU_IdUsuario,0);
			}
		}
        
		$m = md5('5');
		header("Location: ".$ruta."assets/views/usuario/index.php?m=".$m);				
		
	}		
?>