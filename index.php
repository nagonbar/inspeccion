<?php
	$_SESSION['NombreSistema'] = 'inspeccion';
	
	switch ($_GET['error']) {
		case md5(1):
        	$mensaje = '<div class="alert alert-danger" align="center">Usuario o Clave Incorrecta</div>';
			break;
		case md5(2):
	        $mensaje = '<div class="alert alert-success" align="center">Clave enviada a su correo.</div>';
			break;
		case md5(3):
	        $mensaje = '<div class="alert alert-danger" align="center">Acceso Denegado.</div>';
			break;
		case md5(4):
	        $mensaje = '<div class="alert alert-danger" align="center">Tiempo máximo inactivo.</div>';
			break;	
		case md5(5):
	        $mensaje = '<div class="alert alert-danger" align="center">Correo no registrado.</div>';
			break;
		case md5(6):
	        $mensaje = '<div class="alert alert-success" align="center">Registro de Usuario Satisfactorio.</div>';
			break;								
	}
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title><?php echo $_SESSION['NombreSistema']; ?></title>
    <meta name="msapplication-TileColor" content="#5bc0de" />

    <link rel="shortcut icon" href="assets/img/favicon.ico"/>        
    <link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/main.min.css">
    <link rel="stylesheet" href="assets/lib/animate.css/animate.min.css">
    <link rel="stylesheet" href="assets/lib/validationEngine/css/validationEngine.jquery.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/lib/datepicker/css/datepicker.css">      
  </head>
  <body class="login"><br>

      <div class="form-signin" align="center">
      <img src="assets/img/logo.png" width="133" height="108"  alt=""/>
      </div>
  <br>
<br>

    <div class="form-signin">
      <div class="text-center">
        <strong><?php echo $_SESSION['NombreSistema']; ?></strong>
      </div>
      <hr>
      <div class="tab-content">
        <!-- INICIAR SESION ********************************************* -->
        <div id="login" class="tab-pane active">
		<form class="form-horizontal" id="popup-validation" action="assets/controller/c_autenticar.php" method="post" name="form1" autocomplete="off">
	        <?php if(isset($_GET['error'])){ echo $mensaje; } ?>
            
            <div class="input-group input-append">
            <input name="NU_Cedula" type="text" class="form-control top validate[required,custom[integer]]" id="NU_Cedula" placeholder="Nro. Cedula">
            <span class="input-group-addon add-on"><i class="glyphicon glyphicon-user"></i></span> 
            </div>
            
            <div class="input-group input-append">
            <input name="AF_Clave" type="password" class="form-control top validate[required]" id="AF_Clave" placeholder="Clave">
            <span class="input-group-addon add-on"><i class="glyphicon glyphicon-lock"></i></span> 
            </div>
            <div>&nbsp;</div>
            
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" id="submit">Iniciar Sesión</button>
          </form>
        </div>
        <!-- RECUPERAR CLAVE ********************************************* -->
        <div id="forgot" class="tab-pane">
          <form method="post" action="assets/controller/c_usuario.php" id="popup-validation2" name="form2" autocomplete="off">
            <?php if(isset($_GET['error'])){ echo $mensaje; } ?>
			<div class="input-group input-append">
            <input name="AF_Correo" id="AF_Correo" type="text" placeholder="Introduzca su Correo" class="form-control top validate[required,custom[email]]">
            <span class="input-group-addon add-on"><i class="glyphicon glyphicon-search"></i></span> 
            </div>
            <br>
            <button class="btn btn-lg btn-danger btn-block" type="submit" name="Rclave" id="Rclave">Recuperar Clave</button>
          </form>
        </div>
        <!-- REGISTRAR USUARIO ********************************************* -->
        <div id="signup" class="tab-pane">
          <form action="assets/controller/c_usuario.php" id="popup-validation3" autocomplete="off" name="form3" method="post">
          <?php if(isset($_GET['error'])){ echo $mensaje; } ?>
            <input name="NU_Cedula" id="NU_Cedula" type="text" placeholder="Nro. Cedula" class="form-control top validate[required,custom[integer]]">
            <input name="AL_Nombre" id="AL_Nombre" type="text" placeholder="Nombre" class="form-control top validate[required]">
            <input name="AL_Apellido" id="AL_Apellido" type="text" placeholder="Apellido" class="form-control top validate[required]">
            <div class="input-group input-append date" id="dp3" data-date="<?=date("Y/m/d")?>" data-date-format="yyyy-mm-dd">
            <input name="FE_FechaNac" id="FE_FechaNac" type="text" placeholder="Fecha de Nacimiento" class="form-control top validate[required],custom[date]" readonly>
            <span class="input-group-addon add-on"><i class="glyphicon glyphicon-calendar"></i></span> 
            </div>
            <input name="AF_Clave1" id="AF_Clave1" type="password" placeholder="Clave" class="form-control top validate[required]">
            <input name="AF_Clave2" id="AF_Clave2" type="password" placeholder="Repetir Clave" class="form-control top validate[required, equals[AF_Clave1]]">
            <input name="AF_Correo" id="AF_Correo" type="text" placeholder="Correo" class="form-control top validate[required,custom[email]]">
            <input name="AF_Telefono" id="AF_Telefono" type="text" placeholder="Telefono" class="form-control validate[required,custom[phone]]">
            <button class="btn btn-lg btn-success btn-block" type="submit" name="registro" id="registro">Registrese</button>
          </form>
        </div>
      </div>
      <hr>
      <div class="text-center">
        <ul class="list-inline">
          <li> <a class="text-muted" href="#login" data-toggle="tab">Iniciar Sesión</a>  </li>
          <li> <a class="text-muted" href="#forgot" data-toggle="tab">Recuperar Clave</a>  </li>
          <?php /*?><li> <a class="text-muted" href="#signup" data-toggle="tab">Registrese</a>  </li><?*/?>
        </ul>
      </div>
    </div>
    <script src="assets/lib/jquery/jquery.min.js"></script>
    <script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      (function($) {
        $(document).ready(function() {
          $('.list-inline li > a').click(function() {
            var activeForm = $(this).attr('href') + ' > form';
            //console.log(activeForm);
            $(activeForm).addClass('animated fadeIn');
            //set timer to 1 seconds, after that, unload the animate animation
            setTimeout(function() {
              $(activeForm).removeClass('animated fadeIn');
            }, 1000);
          });
        });
      })(jQuery);
    </script>


    <!--jQuery 2.1.1 -->

    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>

    <!--Bootstrap -->


    <!-- Screenfull -->
    <script src="assets/lib/screenfull/screenfull.js"></script>
    <script src="assets/lib/validationEngine/js/jquery.validationEngine.js"></script>
    <script src="assets/lib/validationEngine/js/languages/jquery.validationEngine-en.js"></script>
    <script src="assets/lib/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="assets/lib/jquery-validation/src/localization/messages_ja.js"></script>

    <!-- Metis core scripts -->
    <script src="assets/js/core.js"></script>

    <!-- Metis demo scripts -->
    <script src="assets/js/app.min.js"></script>
    <script>
      $(function() {
        Metis.formValidation();
      });
    </script>
    <script src="assets/js/style-switcher.js"></script>
    
	<!-- DATAPICKER -->
    <script src="assets/lib/jquery.uniform/jquery.uniform.min.js"></script>
    <script src="assets/lib/inputlimiter/jquery.inputlimiter.js"></script>
    <script src="assets/lib/chosen/chosen.jquery.min.js"></script>
    <script src="assets/lib/colorpicker/js/bootstrap-colorpicker.js"></script>
    <script src="assets/lib/tagsinput/jquery.tagsinput.js"></script>
    <script src="assets/lib/validVal/js/jquery.validVal.min.js"></script>
    <script src="assets/lib/datepicker/js/bootstrap-datepicker.js"></script>
    <script>
      $(function() {
        Metis.formGeneral();
      });
    </script>
    <!-- ********************************************************  -->    
  </body>
</html>
